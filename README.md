# Handy tools for (untargeted) NMR metanolomics

## Description

## Installation
Install `devtools` to be able to install packages from Git repositories.

Install `NMRTools` package by:

`devtools::install_git("https://gitlab.com/CarlBrunius/NMRtools.git")`

## Version history
version | date  | comment
:-----: | :---: | :------
0.0.912 | 2016-12-20 | Updated `statNMR` with 1D-STOCSY and method parameters
0.0.911 | 2016-09-21 | `cleanReg` -> `cleanRegion` & `paretoScale`
0.0.910 | 2016-09-20 | Added `cleanReg` to clean up noisy or poorly aligned regions
0.0.900 | 2016-06-04 | First commit. `IndFind`; `findPeaks`; `plotNMR`; `statNMR`
